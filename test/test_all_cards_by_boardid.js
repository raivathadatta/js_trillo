const { key } = require("../consts.js");
const fetchAllCardsByBoardId = require("../problems/fetch_all_card_by_boardid.js");

fetchAllCardsByBoardId(key.boardID).then((data)=>{
    console.log(data)
}).catch((error)=>{
    console.log(error)
})
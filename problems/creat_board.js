const Keys = require('../consts.js');

function createNewBoard(boardName) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/boards/?name=${boardName}&key=${Keys.key.key}&token=${Keys.key.token}`, {
            method: 'POST'
        }).then((response) => {
            resolve(response.text())
        }).catch((err) => reject(err))
    })
}
module.exports = createNewBoard
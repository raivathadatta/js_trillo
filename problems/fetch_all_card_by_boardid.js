const { error } = require("console");
const { key } = require("../consts.js");
const getAllListInBoard = require("../problems/fetch_list.js");
const getCardsByListId = require("./fetch_cards_by_list_id.js");
const { rejects } = require("assert");
function fetchAllCardsByBoardId(boardId) {
    return new Promise((resolve, reject) => {
        getAllListInBoard(boardId).then((data) => {
            let list = JSON.parse(data)
            let listId = []
            listId = list.map((element) => {
                return getCardsByListId(element.id).then((data) => {
                    // console.log(data)
                    return data

                })
            })

            return listId

        }).then((listId) => {
            let card = Promise.all(listId)
            console.log(card)
            return card

        }).then((data) => {


            resolve(data)
        })
    }).catch((error)=>{
        rejects(error)
    })
}
module.exports = fetchAllCardsByBoardId
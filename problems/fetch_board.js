const paths = require('../consts')

function fetchBoard(boardID) {
    return new Promise((response, reject) => {
        fetch(`https://api.trello.com/1/boards/${boardID}?key=${paths.key.key}&token=${paths.key.token}`,
            { method: 'GET', headers: { 'Accept': 'application/json' } })
            .then((data) => {
                response(data.text())
            }).catch((error) => {
                reject(error)
            })
    })
}

module.exports=fetchBoard
const { key } = require("../consts");

function updateState(value) {
    return new Promise((resolve, reject) => {
        if (typeof value === "boolean") {

            fetch(`https://api.trello.com/1/boards/66541b769415c69f80aa6e98/checklists?key=${key.key}&token=${key.token}`, {
                method: 'GET'
            })
                .then(response => {
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                    );

                    return response.text();
                })
                .then(text => {
                    let data = JSON.parse(text)
                    console.log(data)
                    data.forEach(element => {
                        let cardId = element.idCard
                        element['checkItems'].forEach((item) => {
                            console.log(item)
                            let checkListId = item.idChecklist
                            let id = item.id
                            fetch(`https://api.trello.com/1/cards/${cardId}/checklist/${checkListId}/checkItem/${id}/state?key=${key.key}&token=${key.token}&value=${value} `,
                                {
                                    method: 'PUT'
                                })
                                .then(response => {
                                    console.log(
                                        `Response: ${response.status} ${response.statusText}`
                                    );
                                    return response.text();
                                })
                                .then(text =>
                                    console.log(text)
                                )
                                .catch(err => console.error(err));
                        })
                    })
                }
                ).then(() => resolve('success'))
                .catch(err => reject(err));
        } else {
            reject("input missing required boolean parameter")
        }

    })
}


module.exports = updateState

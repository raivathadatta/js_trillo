const getAllidListInBoard = require("../problems/fetch_list.js")
const deleteList = require("../problems/delete_list_promiss.js")

function deleteidListSymultaneously(boardId) {
    return new Promise((resolve, reject) => {
        getAllidListInBoard(boardId).then((data) => {
            data = JSON.parse(data)
            let idList = []
            data.forEach((element) => {
                idList.push(element.id)
            });
            return idList
        }).then((idList) => {
            let promisesList = []
            for (let element of idList) {
                promisesList.push(deleteList(element))
            }
            return Promise.all(promisesList)
        }).then((promisesList) => { 
            console.log(promisesList) 
            resolve(promisesList)

        }).catch((err) => { reject(err) }).finally
    })
}



module.exports = deleteidListSymultaneously
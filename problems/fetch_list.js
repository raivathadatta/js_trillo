const { key } = require("../consts.js")
const fetchBoard = require("./fetch_board.js")

function fetchAllListInBoard(boardId) {
    return new Promise((resolve, reject) => {

        fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${key.key}&token=${key.token}`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json'
                }
            })
            .then((response) => {

                return response.text();
            })
            .then((data) => {
                resolve(data)
            })
            .catch((err) => { reject(err) });
    })
}

module.exports = fetchAllListInBoard
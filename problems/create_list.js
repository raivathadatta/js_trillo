const { key } = require("../consts");

function createList(boardId, listName) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${key.key}&token=${key.token}`, {
            method: 'POST'
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => {
                console.log(text)
                resolve(text)})
            .catch(err => reject(err));
    })
}
module.exports = createList
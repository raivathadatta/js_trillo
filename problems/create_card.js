const { key } = require("../consts");

function createCard(listID,) {
    return new Promise((resolve, reject) => {

        fetch(`https://api.trello.com/1/cards?idList=${listID}&key=${key.key}&token=${key.token}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => resolve(text))
            .catch(err => reject(err));
    })
}
module.exports = createCard
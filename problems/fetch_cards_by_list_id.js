const { key } = require("../consts");

function getCardsByListId(listId) {
    return new Promise((resolve, reject) => {

        fetch(`https://api.trello.com/1/lists/${listId}/cards?key=${key.key}&token=${key.token}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then((data) => resolve(data))
            .catch(err => reject(err));
    })
}

module.exports = getCardsByListId
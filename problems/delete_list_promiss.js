
const {key} = require('../consts')
function deleteList(listID){
    return new Promise((resolve, reject) =>{
      fetch(`https://api.trello.com/1/lists/${listID}/closed?key=${key.key}&token=${key.token}&value=true`, {
        method: 'PUT'
      }).then((response) =>{
        resolve(response.text())
      }).catch((err) =>reject(err))
    })
}

module.exports=deleteList
const createNewBoard = require("./creat_board")
const createCard = require("./create_card")
const createList = require("./create_list")

function createBoardListCard(boardName) {
    return new Promise(function (resolve, reject) {
        createNewBoard("newBoard").then((data) => {
            data = JSON.parse(data)
            return data['id']
        }).then((id) => {
            console.log(id)
            let listIDs = []
            let cards = []
            for (let index = 0; index < 3; index++) {
                let listName = `${boardName}${index}`
                createList(id, listName).then((data) => {
                    console.log(typeof data, "listname")
                    data = JSON.parse(data)
                    console.log(typeof data, "listname")
                    listIDs.push(data['id'])
                    return data['id']
                }).then((id) => {
                    console.log(id)
                    createCard(id).then((data) => {
                        console.log(data)
                        cards.push(data)
                    })
                })
            }
            return cards
        }).then((cards) => {
            resolve(cards)
        }).catch((error) => {
            console.log(error)
            reject(error)
        })
    })
}
module.exports = createBoardListCard